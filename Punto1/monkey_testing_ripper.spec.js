describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomClick(10);
	randomEvent(10);
    })
})
function randomClick(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.Dom.isHidden(randomLink)) {
		try{
	  		cy.wrap(randomLink).click({force: true});
		}catch(e){}
                monkeysLeft = monkeysLeft - 1;
            }
   	    cy.wait(1000);
            randomClick(monkeysLeft);
        });
    }   
}

function randomEvent(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if (monkeysLeft > 0) {

	//Inputs
	var inputs = cy.get('input');
	
	if (inputs != null && inputs.length > 0){	
          inputs.then($input => {
              var randomInput = $input.get(getRandomInt(0, $input.length));
	      if(!Cypress.Dom.isHidden(randomInput)) {	    
	        var esTxt = false;
                var intentos = 0;
	        do{
		  // Inputs de tipo text
		  if (randomInput != null && randomInput.type == 'text'){
        	    cy.wrap(randomInput).click({force: true}).type("prueba",{force: true});
		    esTxt = true;
		  }
	          randomInput = $input.get(getRandomInt(0, $input.length));
	          intentos++;
	        }while(!esTxt && intentos<5);
              }
	    
          });
	}

        // Inputs button
	var buttonAlls = cy.get('button');
	if (buttonAlls != null && buttonAlls.length > 0){
          buttonAlls.then($buttons => {
            var button = $buttons.get(getRandomInt(0, $buttons.length));
            if (!Cypress.Dom.isHidden(button)) {
                cy.wrap(button).click({ force: true });
            }
          });
	}

		//selects
        var selsAll = cy.get('select');
        if (selsAll != null && selsAll.length > 0) {
            selsAll.then($selects => {
                var select = $selects.get(getRandomInt(0, $selects.length));
                if (!Cypress.Dom.isHidden(select)) {
                    cy.wrap(select).select(5, { force: true });
                }
            });
        }

        monkeysLeft = monkeysLeft - 1;
        cy.wait(1000);	
	randomEvent(monkeysLeft);
    }
}
